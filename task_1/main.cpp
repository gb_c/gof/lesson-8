#include <iostream>
#include "state.h"


int main()
{
    StateContext* pStateContext = new (std::nothrow) StateContext(new NoCoinsState());

    if(pStateContext != nullptr)
    {
        pStateContext->Throw();
        pStateContext->Return();
        pStateContext->Throw();
        pStateContext->Buy();
        pStateContext->Throw();
        pStateContext->Buy();
        pStateContext->Throw();
        pStateContext->Buy();
        pStateContext->Throw();
        pStateContext->Buy();

        pStateContext->Throw();
        pStateContext->Buy();

        delete pStateContext;
    }

    return 0;
}
