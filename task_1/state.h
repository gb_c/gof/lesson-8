#ifndef STATE_H
#define STATE_H

#include <iostream>
#include <cstdint>


class StateContext;


class State
{
private:
    std::string name;

protected:
    static uint8_t coins;
    static uint8_t coffee;

public:
    State(const std::string& name) : name(name) {}

    std::string GetName()
    {
        return name;
    }

    virtual void Throw(StateContext*) = 0;
    virtual void Return(StateContext*) = 0;
    virtual void Buy(StateContext*) = 0;
};


uint8_t State::coins = 5u;
uint8_t State::coffee = 3u;


class StateContext
{
private:
    State* state;

public:
    StateContext(State* state)
        : state(state) {}

    void Throw()
    {
        std::cout << "Throwing a coin..." << std::endl;
        state->Throw(this);
    }

    void Return()
    {
        std::cout << "Returning a coin..." << std::endl;
        state->Return(this);
    }

    void Buy()
    {
        std::cout << "Buying coffee..." << std::endl;
        state->Buy(this);
    }

    void SetState(State* s)
    {
        std::cout << "Changing state from " << state->GetName()
        << " to " << s->GetName() << "..." << std::endl;
        delete state;
        state = s;
    }

    State* GetState()
    {
        return state;
    }

    ~StateContext()
    {
        delete state;
    }
};


class NoCoinsState : public State
{
public:
    NoCoinsState() : State("no coins") {}
    virtual void Throw(StateContext* state);
    virtual void Return(StateContext* state);
    virtual void Buy(StateContext* state);
};


class HaveCoinsState : public State
{
public:
    HaveCoinsState() : State("have coins") {}
    virtual void Throw(StateContext* state);
    virtual void Return(StateContext* state);
    virtual void Buy(StateContext* state);
};


class CoffeeSoldState : public State
{
public:
    CoffeeSoldState() : State("coffee sold") {}

    virtual void Throw(StateContext* state);
    virtual void Return(StateContext* state);
    virtual void Buy(StateContext* state);
};


class NoCoffeeState : public State
{
public:
    NoCoffeeState() : State("no coffee") {}
    virtual void Throw(StateContext* state);
    virtual void Return(StateContext* state);
    virtual void Buy(StateContext* state);
};




void NoCoinsState::Throw(StateContext* state)
{
    if(coins > 0)
    {
        coins--;
        state->SetState(new HaveCoinsState);
    }
    else
    {
        std::cout << "You don't have coins" << std::endl;
    }
}

void NoCoinsState::Return(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

void NoCoinsState::Buy(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}


void HaveCoinsState::Throw(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

void HaveCoinsState::Return(StateContext* state)
{
    state->SetState(new NoCoinsState());
    coins++;
}

void HaveCoinsState::Buy(StateContext* state)
{
    if(coffee > 0)
    {
        state->SetState(new CoffeeSoldState());
        coffee--;
        state->SetState(new NoCoinsState());
    }
    else
    {
        coins++;
        state->SetState(new NoCoffeeState());
    }
}


void CoffeeSoldState::Throw(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

void CoffeeSoldState::Return(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

void CoffeeSoldState::Buy(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}


void NoCoffeeState::Throw(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

void NoCoffeeState::Return(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

void NoCoffeeState::Buy(StateContext* state)
{
    std::cout << "Nothing happens" << std::endl;
}

#endif // STATE_H
