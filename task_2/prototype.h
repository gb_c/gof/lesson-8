#ifndef PROTOTYPE_H
#define PROTOTYPE_H

#include <iostream>


class MonsterBase
{
protected:
    int hp;
    int atk;
    int def;

public:
    MonsterBase() : hp(0), atk(0), def(0) {}
    virtual ~MonsterBase() {}
    virtual void changeHp(int change) = 0;
    virtual void changeAtk(int change) = 0;
    virtual void changeDef(int change) = 0;
    virtual MonsterBase* clone() const = 0;
};


class Monster : public MonsterBase
{
private:
    std::string type;

public:
    Monster() : type(0) {}

    Monster(const Monster& o)
    {
        type = o.type;
        hp = o.hp;
        atk = o.atk;
        def = o.def;
    }

    Monster(std::string newType, int newHp, int newAtk, int newDef)
    {
        type = newType;
        hp = newHp;
        atk = newAtk;
        def = newDef;
    }

    void changeHp(int change)
    {
        hp += change;
        std::cout << "Hp changed to " << hp << std::endl;
    }

    void changeAtk(int change)
    {
        atk += change;
        std::cout << "Atk changed to " << atk << std::endl;
    }

    void changeDef(int change)
    {
        def += change;
        std::cout << "Def changed to " << def << std::endl;
    }

    int getHp() const
    {
        return hp;
    }

    int getAtk() const
    {
        return atk;
    }

    int getDef() const
    {
        return def;
    }

    virtual Monster* clone() const
    {
        return new Monster(*this);
    }
};

#endif // PROTOTYPE_H
