#include <vector>
#include "prototype.h"


int main()
{
    std::vector<MonsterBase*> monster;

    monster.push_back(new Monster("Zombie", 100, 20, 30));
    monster.push_back(monster[0]->clone());
    monster.push_back(monster[0]->clone());

    for(MonsterBase *ptr : monster)
    {
        ptr->changeHp(-10);
        ptr->changeAtk(5);
        ptr->changeDef(10);
    }

    for(MonsterBase *ptr : monster)
    {
        delete ptr;
    }

    return 0;
}
